/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    
    @FXML
            
    Connection conn;
    
    private void handleButtonAction(ActionEvent event) {
        doConn();
        doInsert();        
    }
    
    private void doConn(){
        try {
            conn = DriverManager.getConnection("jdbc:derby://localhost:1527/contact", "test", "test");
            if (conn!= null){
            label.setText("Succeeded");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            label.setText("Failed");
        }
    }
    
    private int doInsert(){
        int rows = 0;
        String insert = "insert into TEST.COLLEAGUES values (?,?,?,?,?,?,?)";
        
        PreparedStatement stmt = conn.preparedStatement(insert);
        
    
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
